﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public static GameController Instance;
    public bool isPlayerTurn;
    public bool areEnemiesMoving;
    public int playerCurrentHealth = 50;
    public AudioClip gameOverSound;

    private BoardController boardController;
    private List<Enemy> enemies;
    public GameObject levelImage;
    private Text levelText;
    private bool settingUpGame;
    private int secondsUntilLevelStart = 3;
    private int currentLevel = 1;
    public GameObject levelButton;
    private Text levelStart;
    private int HighScore;
    //private GameObject ;

	void Awake () {
        HighScore = PlayerPrefs.GetInt("highscore");
        if(Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
        boardController = GetComponent<BoardController>();
        enemies = new List<Enemy>();
	}

    void Start ()
    {
        InitializeGame();
        levelText = GameObject.Find("Level Text").GetComponent<Text>();
        levelText.text = "Welcome to my Roguelike game!" + "\n" + "\n" + "\n" + "Will you make it? =)" + "\n" + "\n" + " Day " + currentLevel;
        //levelButton = GameObject.Find("Button");
        //levelStart.text = "Start";
        levelImage = GameObject.Find("Level Image");

    }

    private void InitializeGame()
    {
        settingUpGame = true;
        levelImage = GameObject.Find("Level Image");
        levelText = GameObject.Find("Level Text").GetComponent<Text>();
        levelText.text = "Day " + currentLevel;
        //levelText.text = "Day" + currentLevel;
        levelImage.SetActive(true);
        enemies.Clear();
        boardController.SetupLevel(currentLevel);
        Invoke("DisableLevelImage", secondsUntilLevelStart);
    }
    //GetComponent<Text>();
    private void DisableLevelImage()
    {
        levelImage = GameObject.Find("Level Image");
        //levelButton = GameObject.Find("Button");
        //levelText = GameObject.Find("Level Text").GetComponent<Text>();
        //levelText.text = "Welcome to my Roguelike game!" + "\n" + "Hope you enjoy!" + "\n" + "\n" + "Will you make it? =)" + "\n" + " Day " + currentLevel;
        levelImage.SetActive(false);
        //levelButton.SetActive(false);
        settingUpGame = false;
        isPlayerTurn = true;
        areEnemiesMoving = false;
    }
	
    private void OnLevelWasLoaded(int levelLoaded)
    {
        currentLevel++;
        InitializeGame();
    }

	void Update () {
        if(isPlayerTurn || areEnemiesMoving || settingUpGame)
        {
            return;
        }

        StartCoroutine(MoveEnemies());
	}

    private IEnumerator MoveEnemies()
    {
        areEnemiesMoving = true;

        yield return new WaitForSeconds(0.2f);

        foreach(Enemy enemy in enemies)
        {
            enemy.MoveEnemy();
            yield return new WaitForSeconds(enemy.moveTime);
        }

        areEnemiesMoving = false;
        isPlayerTurn = true;
    }

    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }

    public void GameOver()
    {
        if(HighScore < currentLevel)
        {
            PlayerPrefs.SetInt("highscore", currentLevel);
        }
        HighScore = PlayerPrefs.GetInt("highscore");
        isPlayerTurn = false;
        SoundController.Instance.music.Stop();
        SoundController.Instance.PlaySingle(gameOverSound);
        levelText.text = "You can't" + "\n" + "\n \n Highscore: "+ HighScore+ " Days";
        levelImage.SetActive(true);
        enabled = false;
    }
    public void Button()
    {
        DisableLevelImage();

    }
}
